﻿<p align="center"><img src="https://images.gitee.com/uploads/images/2020/1204/145903_cea2bf9d_302533.png" height="80"/></p>

[中文](README.md) | English

## Introduction

Gardener is a back-end management system developed based on .net 6, the front and back of the system are separated, the api is developed based on the Furion framework, and the front-end is developed based on ant-design-blazor. The system uses a newer technology or framework. Please click star if you like :kissing_heart: 

## Demo address
UserName：admin、admin1、admin2、admin3、admin4、admin5、admin6

Password：admin

Just wait a moment when you first time to open it. :two_hearts: [http://47.94.212.176:1000](http://47.94.212.176:1000)  
Please do not delete user data casually. The demo service is initialized randomly. It is recommended to run docker to check.

 **run from docker** 
```
docker pull huhangfei/gardener
docker run --name gardener -p 80:80 --restart=always -d huhangfei/gardener
```

## feature
- NEW：.Net6 、Blazor WebAssembly 、Furion : all new technologies。
- EASY：Simple and practical function

## Documentation
[Documentation](https://gitee.com/hgflydream/Gardener/wikis)

## Contributor

Thanks to everyone who contributed code to  **Gardener** , and welcome everyone to submit a PR or Issue.

## Links
👉 **[Furion](https://gitee.com/dotnetchina/Furion)**  
👉 **[ant-design-blazor](https://github.com/ant-design-blazor/ant-design-blazor)**

## Follow us

 **qq group**

<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=ILV3MBrcZtr4uUSsKa3njjnpBiUvT0xe&jump_from=webapi">
<img alt="click to join" title="click to join" src="https://images.gitee.com/uploads/images/2021/1101/112200_a6d329a3_302533.png" width="200px" height="200px"/>
</a>