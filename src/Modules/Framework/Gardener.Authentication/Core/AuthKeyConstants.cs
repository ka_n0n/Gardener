﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

namespace Gardener.Authentication.Core
{
    /// <summary>
    /// 
    /// </summary>
    public static class AuthKeyConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly string ClientIdKeyName = "loginId";
        /// <summary>
        /// 
        /// </summary>
        public static readonly string ClientTypeKeyName = "clientType";
        /// <summary>
        /// 
        /// </summary>
        public static readonly string TokenTypeKey = "tokenTypeKey";
        /// <summary>
        /// 
        /// </summary>
        public static readonly string UserIsSuperAdministratorKey = "IsSuperAdministrator";
        /// <summary>
        /// 
        /// </summary>
        public static readonly string IdentityType = "identityType";
    }
}
